#!/bin/bash

readonly CI_DIR=$ROOT_DIR/ci
readonly CLANG=/usr/bin/clang-9

readonly ASAN_DIR=$CI_DIR/asan
readonly TSAN_DIR=$CI_DIR/tsan
readonly RELEASE_DIR=$CI_DIR/release
readonly STATIC_ANALYSIS_DIR=$CI_DIR/static_analysis
readonly REPORTS_DIR=$CI_DIR/reports
readonly TIDY_DIR=$CI_DIR/tidy
