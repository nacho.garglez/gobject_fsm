#!/bin/bash

set -e

readonly ROOT_DIR="$(realpath $( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )/../)"
source $ROOT_DIR/scripts/common.sh

rm -rf $RELEASE_DIR
meson $RELEASE_DIR -Dbuildtype=debugoptimized
ninja -C $RELEASE_DIR
