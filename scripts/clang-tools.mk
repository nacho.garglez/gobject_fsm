MAKEFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
ROOT_DIR := $(patsubst %/,%,$(dir $(patsubst %/,%,$(dir $(MAKEFILE_PATH)))))

C_FILES := $(wildcard $(ROOT_DIR)/src/*.c $(ROOT_DIR)/tests/*.c)
H_FILES := $(wildcard $(ROOT_DIR)/src/*.h $(ROOT_DIR)/include/*.h)
FILES := $(C_FILES) $(H_FILES)

CLANG_FORMAT ?= clang-format-9
CLANG_FORMAT_FILES := $(foreach file,$(FILES),$(file).format)
CLANG_CHECK_FORMAT_FILES := $(foreach file,$(FILES),$(file).check_format)

CLANG_TIDY ?= clang-tidy-9
CLANG_TIDY_FILES := $(foreach file,$(FILES),$(file).tidy)

# Pick any flags you like here
CLANG_TIDY_CHECKS = readability-identifier-naming

# https://clang.llvm.org/extra/clang-tidy/checks/readability-identifier-naming.html

CLANG_TIDY_CONFIG = {CheckOptions: [\
{key: readability-identifier-naming.EnumCase, value: CamelCase},\
{key: readability-identifier-naming.FunctionCase, value: lower_case},\
{key: readability-identifier-naming.GlobalFunctionPrefix, value: fsm_},\
{key: readability-identifier-naming.VariableCase, value: lower_case},\
{key: readability-identifier-naming.ParameterCase, value: lower_case},\
{key: readability-identifier-naming.PublicMemberCase, value: lower_case},\
{key: readability-identifier-naming.TypedefCase, value: CamelCase},\
{key: readability-identifier-naming.StructCase, value: CamelCase}\
]}

TIDY_DIR ?= $(ROOT_DIR)/ci/tidy
CLANG_TIDY_CC = $(TIDY_DIR)/compile_commands.json
CLANG_TIDY_TMP_DIR = $(abspath $(TIDY_DIR)/../tidy_tmp)
CLANG_TIDY_TMP_CC = $(CLANG_TIDY_TMP_DIR)/compile_commands.json

format: $(CLANG_FORMAT_FILES)

%.format:
	@$(CLANG_FORMAT) -i -style=file --assume-filename=$(ROOT_DIR)/.clang-format $*

check-format: $(CLANG_CHECK_FORMAT_FILES)

%.check_format:
	@$(CLANG_FORMAT) -style=file --assume-filename=$(ROOT_DIR)/.clang-format $* | diff -u $* -

tidy: $(CLANG_TIDY_FILES)
	rm -rf $(CLANG_TIDY_TMP_DIR)

%.tidy: $(CLANG_TIDY_TMP_CC)
	@$(CLANG_TIDY) -config="$(CLANG_TIDY_CONFIG)"  -header-filter='fsm*' -checks="$(CLANG_TIDY_CHECKS)" -warnings-as-errors=* -p $(CLANG_TIDY_TMP_DIR) $*

$(CLANG_TIDY_TMP_CC):
	mkdir -p $(dir $@)
	cp $(CLANG_TIDY_CC) $@
	sed -i 's/-pipe//g' $@

dbg-%:
	@echo "$* = $($*)"

.PHONY: format tidy
