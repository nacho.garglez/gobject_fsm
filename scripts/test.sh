#!/bin/bash

set -e

readonly ROOT_DIR="$(realpath $( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )/../)"
source $ROOT_DIR/scripts/common.sh

mkdir -p $REPORTS_DIR

# Address and undefined sanitizer
rm -rf $ASAN_DIR
CC=$CLANG meson $ASAN_DIR -Dbuildtype=debugoptimized -Db_sanitize=address,undefined -Db_lundef=false -Dstatic_thread_analysis=true

# Check format
echo -e "\n=== CHECK FORMAT ===\n"
ninja -C $ASAN_DIR check-format

# Static analysis with scan-build
rm -rf $STATIC_ANALYSIS_DIR
echo -e "\n=== STATIC ANALYSIS - SCAN BUILD ===\n"
CC=$CLANG scan-build-9 --use-cc=$CLANG --force-analyze-debug-code --use-analyzer=$CLANG -o $STATIC_ANALYSIS_DIR ninja -C $ASAN_DIR scan-build
report=$(ls $STATIC_ANALYSIS_DIR | wc -l)
if [[ $report -ne 0 ]]; then
    echo "ERRORS FOUND DURING STATIC ANALYSIS"
    cp $(find $STATIC_ANALYSIS_DIR -iname "report*.html") $REPORTS_DIR/static_analysis.html
    exit 1
fi

echo -e "\n=== TEST - CLANG ADDRESS AND UNDEFINED SANITIZER ===\n"
ninja -C $ASAN_DIR test

# Clang tidy
rm -rf $TIDY_DIR
echo -e "\n=== STATIC ANALYSIS - CLANG TIDY ===\n"
CC=$CLANG meson $TIDY_DIR
ninja -C $TIDY_DIR tidy

# Thread sanitizer
if [[ $(dpkg -l libglibtsan2.0-0 > /dev/null) ]]; then
    echo -e "\n=== TEST - CLANG THREAD SANITIZER ===\n"
    rm -rf $TSAN_DIR
    PKG_CONFIG_PATH=/opt/tsan/lib/x86_64-linux-gnu/pkgconfig/:$PKG_CONFIG_PATH  \
    CC=$CLANG meson $TSAN_DIR -Dbuildtype=debugoptimized -Db_sanitize=thread -Db_lundef=false
    TSAN_OPTIONS="suppressions=/opt/tsan/share/glib-2.0/tsan/glib.supp"  ninja -C $TSAN_DIR test
else
    echo -e "\n=== TEST - SKIPPED CLANG THREAD SANITIZER ===\n"
fi

# Release with coverage support
rm -rf $RELEASE_DIR
meson $RELEASE_DIR -Dbuildtype=debugoptimized -Db_coverage=true
echo -e "\n=== TEST - GCC RELEASE ===\n"
ninja -C $RELEASE_DIR test

# Test coverage with memory sanitizer build
ninja -C $RELEASE_DIR coverage
echo -e "\n=== GCC COVERAGE ===\n"
cat $RELEASE_DIR/meson-logs/coverage.txt
cp $RELEASE_DIR/meson-logs/coverage.xml $RELEASE_DIR/meson-logs/coverage.txt $REPORTS_DIR/
cp -R $RELEASE_DIR/meson-logs/coveragereport $REPORTS_DIR

# Valgrind
cd $RELEASE_DIR

# Memory leak detector
echo -e "\n=== TEST - VALGRIND MEMORY SANITIZER ===\n"
meson test --wrap="valgrind --tool=memcheck --leak-check=full --suppressions=$ROOT_DIR/tests/valgrind_memory.supp --error-exitcode=1"

# Thread error detectors
# Disable for now because neither Helgrind nor DRD understand glib directives
#echo -e "\n=== TEST - VALGRIND THREAD SANITIZER (HELGRIND) ===\n"
#meson test --wrap="valgrind --tool=helgrind --error-exitcode=1"
#echo -e "\n=== TEST - VALGRIND THREAD SANITIZER (DRD) ===\n"
#meson test --wrap="valgrind --tool=drd --error-exitcode=1"
