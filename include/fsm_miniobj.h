/*
 * GFsm - useful State Machine Library for GLIB
 * Copyright (C) 2020 Nacho Garcia, Pablo Marcos, and others.
 * See AUTHORS file for a complete list.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __FSM_MINIOBJ_H__
#define __FSM_MINIOBJ_H__

#include "fsm_refcount.h"

#include <glib-object.h>

#define FSM_MINIOBJ_CAST(obj)              ((FsmMiniObj *) (obj))
#define FSM_MINIOBJ_CONST_CAST(obj)        ((const FsmMiniObj *) (obj))
#define FSM_MINIOBJ(obj)                   (FSM_MINIOBJ_CAST (obj))
#define FSM_MINIOBJ_SET_FREE_FNC(obj, fnc) (FSM_MINIOBJ (obj)->free = fnc)
#define FSM_MINIOBJ_GET_REFCOUNT(obj)      (FSM_MINIOBJ (obj)->ref_count)

typedef struct _FsmMiniObj FsmMiniObj;
typedef void (*FsmMiniObjFreeFunction) (FsmMiniObj *);

struct _FsmMiniObj
{
  GType type;
  FsmAtomicRefCount ref_count;
  FsmMiniObjFreeFunction free;
};

FsmMiniObj *fsm_miniobj_ref (FsmMiniObj *);
void fsm_miniobj_unref (FsmMiniObj *);
void fsm_miniobj_init (FsmMiniObj *, GType);

#define GST_DEFINE_FSM_MINIOBJ_OBJECT_TYPE(TypeName, type_name)                \
  G_DEFINE_BOXED_TYPE (TypeName, type_name, (GBoxedCopyFunc) fsm_miniobj_ref,  \
                       (GBoxedFreeFunc) fsm_miniobj_unref)

#endif /*__FSM_MINIOBJ_H__*/
