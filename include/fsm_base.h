/*
 * GFsm - useful State Machine Library for GLIB
 * Copyright (C) 2020 Nacho Garcia, Pablo Marcos, and others.
 * See AUTHORS file for a complete list.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __FSM_BASE_H__
#define __FSM_BASE_H__

#include "fsm_event_data.h"

#include <glib-object.h>

#define FSM_BASE_TYPE (fsm_base_get_type ())
#define FSM_BASE(obj)                                                          \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), FSM_BASE_TYPE, FsmBase))
#define IS_FSM_BASE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), FSM_BASE_TYPE))
#define FSM_BASE_CLASS(klass)                                                  \
  (G_TYPE_CHECK_CLASS_CAST ((klass), FSM_BASE_TYPE, FsmBaseClass))
#define IS_FSM_BASE_CLASS(klass)                                               \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), FSM_BASE_TYPE))
#define FSM_BASE_GET_CLASS(obj)                                                \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), FSM_BASE_TYPE, FsmBaseClass))

typedef struct _FsmBase FsmBase;
typedef struct _FsmBaseClass FsmBaseClass;
typedef struct _FsmBasePrivate FsmBasePrivate;

typedef void (*FsmBaseExec) (FsmBase *self, FsmEventData *);
typedef void (*FsmBaseEntry) (FsmBase *self, FsmEventData *);
typedef void (*FsmBaseLeave) (FsmBase *self);
typedef gboolean (*FsmBaseGuard) (FsmBase *self, FsmEventData *);

typedef struct _FsmBaseStateDef
{
  FsmBaseExec exec;
  FsmBaseEntry enter;
  FsmBaseLeave leave;
  FsmBaseGuard guard;
} FsmBaseStateDef;

typedef guint FsmBaseStateID;

typedef struct _FsmBaseStatesMap
{
  const FsmBaseStateDef *states;
  const size_t states_size;
} FsmBaseStatesMap;

struct _FsmBase
{
  GObject parent;
  /*< private > */
  FsmBasePrivate *priv;
};

struct _FsmBaseClass
{
  GObjectClass parent;
  const FsmBaseStatesMap *state_map;
};

/*< public > */

GType fsm_base_get_type (void);
FsmBaseStateID fsm_get_current_state (const FsmBase *);
void
fsm_base_external_event (FsmBase *, const FsmBaseStateID *, FsmEventData *);

/* < protected > */
void fsm_base_internal_event (FsmBase *self,
                              FsmBaseStateID new_state,
                              FsmEventData *event_data);

#define INTERNAL_EVENT(self, event, eventdata)                                 \
  fsm_base_internal_event (FSM_BASE (self), event, eventdata);

#define BEGIN_STATE_MAP(_objname_)                                             \
  static const FsmBaseStateDef _objname_##state_map[] = {

#define STATE_ALL(_objname_, _state_name_)                                     \
  { _objname_##_state_##_state_name_##_exec,                                   \
    _objname_##_state_##_state_name_##_enter,                                  \
    _objname_##_state_##_state_name_##_leave,                                  \
    _objname_##_state_##_state_name_##_guard },

#define STATE_FULL(_exec_, _enter_, _leave_, _guard_)                          \
  { _exec_, _enter_, _leave_, _guard_ },

#define STATE(_objname_, _state_name_)                                         \
  { _objname_##_state_##_state_name_##_exec, NULL, NULL, NULL },

#define END_STATE_MAP(_objname_, _FsmBaseClassPtr_)                            \
  }                                                                            \
  ;                                                                            \
  static const FsmBaseStatesMap _objname_##_statesmap = {                      \
    _objname_##state_map,                                                      \
    sizeof (_objname_##state_map) / sizeof (_objname_##state_map[0])           \
  };                                                                           \
  _FsmBaseClassPtr_->state_map = &_objname_##_statesmap;

enum
{
  FSM_BASE_STATE_IGNORED = 0xFFFFE,
  FSM_BASE_STATE_CANNOT_HAPPEN = 0xFFFFF
};

#define BEGIN_EVENT_TRANSITION static const FsmBaseStateID _transitions[] = {

#define TRANSITION(_state_) _state_,

#define TRANSITION_CANNOT_HAPPEN FSM_BASE_STATE_CANNOT_HAPPEN,

#define TRANSITION_IGNORED FSM_BASE_STATE_IGNORED,

#define SUBMIT_EVENT_TRANSITION_SYNC(_objptr_, _last_state_, __data__)         \
  }                                                                            \
  ;                                                                            \
  G_STATIC_ASSERT_EXPR (_last_state_ ==                                        \
                        sizeof (_transitions) / sizeof (_transitions[0]));     \
  fsm_base_external_event (FSM_BASE (_objptr_), _transitions,                  \
                           FSM_EVENT_DATA (__data__));

#endif /* __FSM_BASE_H__ */
