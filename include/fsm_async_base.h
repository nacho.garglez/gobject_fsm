/*
 * GFsm - useful State Machine Library for GLIB
 * Copyright (C) 2020 Nacho Garcia, Pablo Marcos, and others.
 * See AUTHORS file for a complete list.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __FSM_ASYNC_BASE_H__
#define __FSM_ASYNC_BASE_H__

#include "fsm_base.h"

#include <glib-object.h>

#define FSM_ASYNC_BASE_TYPE (fsm_async_base_get_type ())
#define FSM_ASYNC_BASE(obj)                                                    \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), FSM_ASYNC_BASE_TYPE, FsmAsyncBase))
#define IS_FSM_ASYNC_BASE(obj)                                                 \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), FSM_ASYNC_BASE_TYPE))
#define FSM_ASYNC_BASE_CLASS(klass)                                            \
  (G_TYPE_CHECK_CLASS_CAST ((klass), FSM_ASYNC_BASE_TYPE, FsmAsyncBaseClass))
#define IS_FSM_ASYNC_BASE_CLASS(klass)                                         \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), FSM_ASYNC_BASE_TYPE))
#define FSM_ASYNC_BASE_GET_CLASS(obj)                                          \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), FSM_ASYNC_BASE_TYPE, FsmAsyncBaseClass))

typedef struct _FsmAsyncBase FsmAsyncBase;
typedef struct _FsmAsyncBaseClass FsmAsyncBaseClass;
typedef struct _FsmAsyncBasePrivate FsmAsyncBasePrivate;

struct _FsmAsyncBase
{
  FsmBase parent;
  /*< private > */
  FsmAsyncBasePrivate *priv;
};

struct _FsmAsyncBaseClass
{
  FsmBaseClass parent;
};

GType fsm_async_base_get_type (void);
void fsm_async_base_external_event (FsmAsyncBase *,
                                    const FsmBaseStateID *,
                                    FsmEventData *);

#define SUBMIT_EVENT_TRANSITION_ASYNC(_objptr_, _last_state_, __data__)        \
  }                                                                            \
  ;                                                                            \
  G_STATIC_ASSERT_EXPR (_last_state_ ==                                        \
                        sizeof (_transitions) / sizeof (_transitions[0]));     \
  fsm_async_base_external_event (FSM_ASYNC_BASE (_objptr_), _transitions,      \
                                 FSM_EVENT_DATA (__data__));

#endif /* __FSM_ASYNC_BASE_H__ */
