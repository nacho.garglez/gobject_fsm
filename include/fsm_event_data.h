/*
 * GFsm - useful State Machine Library for GLIB
 * Copyright (C) 2020 Nacho Garcia, Pablo Marcos, and others.
 * See AUTHORS file for a complete list.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __fsm_event_data__
#define __fsm_event_data__

#include "fsm_miniobj.h"

#include <glib-object.h>
#include <glib.h>

#define FSM_EVENT_DATA_CAST(obj) ((FsmEventData *) (obj))
#define FSM_EVENT_DATA(obj)      (FSM_EVENT_DATA_CAST (obj))

#define FSM_EVENT_DATA_SET_FREE_FNC(obj, fnc)                                  \
  (FSM_MINIOBJ (obj)->free = (FsmMiniObjFreeFunction) fnc)
#define FSM_EVENT_DATA_TYPE(obj) (FSM_MINIOBJ (obj)->type)

typedef struct _FsmEventData FsmEventData;

struct _FsmEventData
{
  FsmMiniObj parent;
};

void fsm_event_data_init (FsmEventData *self, GType type);
FsmEventData *fsm_event_data_ref (FsmEventData *self);
void fsm_event_data_unref (FsmEventData *self);

#define GST_DEFINE_FSM_EVENT_DATA_OBJECT_TYPE(TypeName, type_name)             \
  G_DEFINE_BOXED_TYPE (TypeName, type_name, (GBoxedCopyFunc) fsm_miniobj_ref,  \
                       (GBoxedFreeFunc) fsm_miniobj_unref)

#endif /*__fsm_event_data__*/
