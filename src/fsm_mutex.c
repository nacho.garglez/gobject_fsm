/*
 * GFsm - useful State Machine Library for GLIB
 * Copyright (C) 2020 Nacho Garcia, Pablo Marcos, and others.
 * See AUTHORS file for a complete list.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifdef STATIC_THREAD_ANALYSIS

#define FSM_MUTEX_IMPL
#include "fsm_mutex.h"

void
fsm_mutex_init (FsmMutex *mutex)
{
  g_mutex_init (&mutex->lock);
}

void
fsm_mutex_clear (FsmMutex *mutex)
{
  g_mutex_clear (&mutex->lock);
}

void
fsm_mutex_lock (FsmMutex *mutex)
{
  g_mutex_lock (&mutex->lock);
}

void
fsm_mutex_unlock (FsmMutex *mutex)
{
  g_mutex_unlock (&mutex->lock);
}

#endif // STATIC_THREAD_ANALYSIS
