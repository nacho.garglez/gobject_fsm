#ifndef _ATOMIC_H_
#define _ATOMIC_H_

/* Glib's atomics are based on volatile an runtime
   and are not detected by clang's thread sanitizer.

   C11 atomics are faster and are correctly detected.

   Use C11 if available
*/

#if __STDC_VERSION__ >= 201112L
#define FSM_ATOMIC               _Atomic
#define FSM_ATOMIC_INT_GET(a)    (*a);
#define FSM_ATOMIC_INT_SET(a, v) ((*a) = v);
#else
#define FSM_ATOMIC               volatile
#define FSM_ATOMIC_INT_GET(a)    g_atomic_int_get (a);
#define FSM_ATOMIC_INT_SET(a, v) g_atomic_int_get (a, v);
#endif

#endif /*_ATOMIC_H_*/