/*
 * GFsm - useful State Machine Library for GLIB
 * Copyright (C) 2020 Nacho Garcia, Pablo Marcos, and others.
 * See AUTHORS file for a complete list.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "fsm_async_base.h"

struct _FsmAsyncBasePrivate
{
  GAsyncQueue *queue;
  GThread *queue_thread;
};

#define FSM_ASYNC_BASE_GET_PRIVATE(obj)                                        \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), FSM_ASYNC_BASE_TYPE,                    \
                                FsmAsyncBasePrivate))

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (FsmAsyncBase,
                                     fsm_async_base,
                                     FSM_BASE_TYPE);

static void fsm_async_base_finalize (GObject *self);
static gpointer queue_thread (gpointer data);

/*
 *  Internal event for terminating queue
 */

typedef struct
{
  FsmMiniObj base;
} TerminateEvent;

GST_DEFINE_FSM_MINIOBJ_OBJECT_TYPE (TerminateEvent, terminate_event);

static TerminateEvent *
terminate_event_new (void)
{
  TerminateEvent *new = g_new0 (TerminateEvent, 1);
  fsm_miniobj_init (FSM_MINIOBJ (new), terminate_event_get_type ());
  return new;
}

/*
 *  Internal event that wraps event data and transition map
 */

typedef struct
{
  FsmMiniObj base;
  const FsmBaseStateID *transition;
  FsmEventData *event_data;
} AsyncEvent;

GST_DEFINE_FSM_MINIOBJ_OBJECT_TYPE (AsyncEvent, async_event);

static void
async_event_free (FsmMiniObj *miniobj)
{
  AsyncEvent *self = (AsyncEvent *) miniobj;
  if (self->event_data)
    fsm_event_data_unref (self->event_data);
  g_free (self);
}

static AsyncEvent *
async_event_new (const FsmBaseStateID *transition, FsmEventData *event_data)
{
  AsyncEvent *new = g_new0 (AsyncEvent, 1);
  fsm_miniobj_init (FSM_MINIOBJ (new), async_event_get_type ());
  if (event_data)
    {
      new->event_data = fsm_event_data_ref (event_data);
    }
  new->transition = transition;
  FSM_MINIOBJ_SET_FREE_FNC (new, async_event_free);
  return new;
}

/*
 * FSM async implementation
 */

static void
fsm_constructed (GObject *self)
{
  FsmAsyncBasePrivate *priv;
  priv = FSM_ASYNC_BASE_GET_PRIVATE (self);
  priv->queue_thread = g_thread_new ("fsm_queue_thread", queue_thread, self);
}

static void
fsm_async_base_class_init (FsmAsyncBaseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = fsm_async_base_finalize;
  object_class->constructed = fsm_constructed;
}

static void
fsm_async_base_init (FsmAsyncBase *self)
{
  FsmAsyncBasePrivate *priv;
  self->priv = priv = FSM_ASYNC_BASE_GET_PRIVATE (self);
  priv->queue = g_async_queue_new ();
}

static void
fsm_async_base_finalize (GObject *self)
{
  FsmAsyncBasePrivate *priv;
  priv = FSM_ASYNC_BASE_GET_PRIVATE (self);
  g_async_queue_push (priv->queue, terminate_event_new ());
  g_thread_join (priv->queue_thread);
  g_async_queue_unref (priv->queue);
  G_OBJECT_CLASS (fsm_async_base_parent_class)->finalize (self);
}

static gpointer
queue_thread (gpointer data)
{
  FsmAsyncBase *self = (FsmAsyncBase *) data;
  FsmAsyncBasePrivate *priv;
  priv = FSM_ASYNC_BASE_GET_PRIVATE (self);
  gboolean finalize = FALSE;

  while (!finalize)
    {
      FsmEventData *event = (FsmEventData *) g_async_queue_pop (priv->queue);
      if (G_UNLIKELY (FSM_EVENT_DATA_TYPE (event) ==
                      terminate_event_get_type ()))
        {
          finalize = TRUE;
        }
      else
        {
          g_assert (FSM_EVENT_DATA_TYPE (event) == async_event_get_type ());
          AsyncEvent *async_event = (AsyncEvent *) event;
          fsm_base_external_event ((FsmBase *) self, async_event->transition,
                                   async_event->event_data);
        }
      fsm_event_data_unref (event);
    }

  return NULL;
}

void
fsm_async_base_external_event (FsmAsyncBase *self,
                               const FsmBaseStateID *transition,
                               FsmEventData *event)
{
  FsmAsyncBasePrivate *priv = FSM_ASYNC_BASE_GET_PRIVATE (self);
  g_async_queue_push (priv->queue, async_event_new (transition, event));
}