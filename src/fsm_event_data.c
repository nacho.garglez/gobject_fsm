/*
 * GFsm - useful State Machine Library for GLIB
 * Copyright (C) 2020 Nacho Garcia, Pablo Marcos, and others.
 * See AUTHORS file for a complete list.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "fsm_event_data.h"

void
fsm_event_data_init (FsmEventData *self, GType type)
{
  fsm_miniobj_init (FSM_MINIOBJ (self), type);
}

FsmEventData *
fsm_event_data_ref (FsmEventData *self)
{
  return (FsmEventData *) fsm_miniobj_ref (FSM_MINIOBJ (self));
}

void
fsm_event_data_unref (FsmEventData *self)
{
  fsm_miniobj_unref (FSM_MINIOBJ (self));
}
