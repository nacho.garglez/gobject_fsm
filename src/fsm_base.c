/*
 * GFsm - useful State Machine Library for GLIB
 * Copyright (C) 2020 Nacho Garcia, Pablo Marcos, and others.
 * See AUTHORS file for a complete list.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "fsm_base.h"

#include "fsm_atomics.h"

#ifdef STATIC_THREAD_ANALYSIS
#include "fsm_mutex.h"
#endif

#define FSM_BASE_GET_PRIVATE(obj)                                              \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), FSM_BASE_TYPE, FsmBasePrivate))

struct _FsmBasePrivate
{
  FsmBaseStateID new_state;
  FSM_ATOMIC FsmBaseStateID current_state;
  FsmEventData *event_data;
  gboolean event_generated;
  GMutex event_mutex;
};

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (FsmBase, fsm_base, G_TYPE_OBJECT);

static void fsm_base_finalize (GObject *self);

static void
fsm_base_class_init (FsmBaseClass *klass)
{

  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = fsm_base_finalize;
}

static void
fsm_base_init (FsmBase *self)
{
  FsmBasePrivate *priv;
  self->priv = priv = FSM_BASE_GET_PRIVATE (self);
  g_mutex_init (&priv->event_mutex);
}

static void
fsm_base_finalize (GObject *self)
{
  FsmBasePrivate *priv;
  priv = FSM_BASE_GET_PRIVATE (self);
  g_mutex_clear (&priv->event_mutex);
  G_OBJECT_CLASS (fsm_base_parent_class)->finalize (self);
}

void
fsm_base_internal_event (FsmBase *self,
                         FsmBaseStateID new_state,
                         FsmEventData *event_data)
{
  FsmBasePrivate *priv = FSM_BASE_GET_PRIVATE (self);
  FsmBaseClass *klass = FSM_BASE_GET_CLASS (self);
  g_assert (new_state < klass->state_map->states_size);
  priv->new_state = new_state;
  priv->event_data = event_data;
  priv->event_generated = TRUE;
}

static inline const FsmBaseStateDef *
get_map_for (FsmBase *self, FsmBaseStateID state)
{
  FsmBaseClass *klass = FSM_BASE_GET_CLASS (self);
  return &klass->state_map->states[state];
}

static inline void
set_current_state (FsmBase *self, FsmBaseStateID state)
{
  FsmBasePrivate *priv = FSM_BASE_GET_PRIVATE (self);
  FSM_ATOMIC_INT_SET (&priv->current_state, state);
}

static void
process_internal (FsmBase *self)
{
  FsmBasePrivate *priv = FSM_BASE_GET_PRIVATE (self);

  while (priv->event_generated)
    {
      FsmEventData *event_data = priv->event_data;

      priv->event_data = NULL;
      priv->event_generated = FALSE;

      const FsmBaseStateDef *current_state_map =
          get_map_for (self, priv->current_state);

      const FsmBaseStateDef *new_state_map =
          get_map_for (self, priv->new_state);

      gboolean guard_result = TRUE;

      if (new_state_map->guard)
        {
          guard_result = new_state_map->guard (self, event_data);
        }

      if (G_LIKELY (guard_result))
        {
          if (current_state_map->leave)
            current_state_map->leave (self);

          if (new_state_map->enter)
            new_state_map->enter (self, event_data);
          g_assert (!priv->event_generated);

          set_current_state (self, priv->new_state);

          g_assert (get_map_for (self, priv->current_state)->exec != NULL);
          get_map_for (self, priv->current_state)->exec (self, event_data);
        }

      if (event_data)
        fsm_event_data_unref (event_data);
    }
}

void
fsm_base_external_event (FsmBase *self,
                         const FsmBaseStateID *transition_map,
                         FsmEventData *event_data)
{
  FsmBasePrivate *priv = FSM_BASE_GET_PRIVATE (self);
  g_mutex_lock (&priv->event_mutex);
  FsmBaseStateID new_state = transition_map[priv->current_state];

  if (new_state == FSM_BASE_STATE_IGNORED)
    {
      if (event_data)
        fsm_event_data_unref (event_data);
    }
  else
    {
      fsm_base_internal_event (self, new_state, event_data);
      process_internal (self);
    }
  g_mutex_unlock (&priv->event_mutex);
}

FsmBaseStateID
fsm_get_current_state (const FsmBase *self)
{
  const FsmBasePrivate *priv = FSM_BASE_GET_PRIVATE (self);
  return FSM_ATOMIC_INT_GET (&priv->current_state);
}
