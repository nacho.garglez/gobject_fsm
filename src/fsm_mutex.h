/*
 * GFsm - useful State Machine Library for GLIB
 * Copyright (C) 2020 Nacho Garcia, Pablo Marcos, and others.
 * See AUTHORS file for a complete list.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FSM_MUTEX_H
#define FSM_MUTEX_H

#ifdef STATIC_THREAD_ANALYSIS

#include <glib.h>

// Enable thread safety attributes only with clang.
// The attributes can be safely erased when compiling with other compilers.
#if defined(__clang__) && (!defined(SWIG))
#define THREAD_ANNOTATION_ATTRIBUTE__(x) __attribute__ ((x))
#else
#define THREAD_ANNOTATION_ATTRIBUTE__(x) // no-op
#endif

#define CAPABILITY(x) THREAD_ANNOTATION_ATTRIBUTE__ (capability (x))

#define SCOPED_CAPABILITY THREAD_ANNOTATION_ATTRIBUTE__ (scoped_lockable)

#define GUARDED_BY(x) THREAD_ANNOTATION_ATTRIBUTE__ (guarded_by (x))

#define PT_GUARDED_BY(x) THREAD_ANNOTATION_ATTRIBUTE__ (pt_guarded_by (x))

#define ACQUIRED_BEFORE(...)                                                   \
  THREAD_ANNOTATION_ATTRIBUTE__ (acquired_before (__VA_ARGS__))

#define ACQUIRED_AFTER(...)                                                    \
  THREAD_ANNOTATION_ATTRIBUTE__ (acquired_after (__VA_ARGS__))

#define REQUIRES(...)                                                          \
  THREAD_ANNOTATION_ATTRIBUTE__ (requires_capability (__VA_ARGS__))

#define REQUIRES_SHARED(...)                                                   \
  THREAD_ANNOTATION_ATTRIBUTE__ (requires_shared_capability (__VA_ARGS__))

#define ACQUIRE(...)                                                           \
  THREAD_ANNOTATION_ATTRIBUTE__ (acquire_capability (__VA_ARGS__))

#define ACQUIRE_SHARED(...)                                                    \
  THREAD_ANNOTATION_ATTRIBUTE__ (acquire_shared_capability (__VA_ARGS__))

#define RELEASE(...)                                                           \
  THREAD_ANNOTATION_ATTRIBUTE__ (release_capability (__VA_ARGS__))

#define RELEASE_SHARED(...)                                                    \
  THREAD_ANNOTATION_ATTRIBUTE__ (release_shared_capability (__VA_ARGS__))

#define TRY_ACQUIRE(...)                                                       \
  THREAD_ANNOTATION_ATTRIBUTE__ (try_acquire_capability (__VA_ARGS__))

#define TRY_ACQUIRE_SHARED(...)                                                \
  THREAD_ANNOTATION_ATTRIBUTE__ (try_acquire_shared_capability (__VA_ARGS__))

#define EXCLUDES(...)                                                          \
  THREAD_ANNOTATION_ATTRIBUTE__ (locks_excluded (__VA_ARGS__))

#define ASSERT_CAPABILITY(x)                                                   \
  THREAD_ANNOTATION_ATTRIBUTE__ (assert_capability (x))

#define ASSERT_SHARED_CAPABILITY(x)                                            \
  THREAD_ANNOTATION_ATTRIBUTE__ (assert_shared_capability (x))

#define RETURN_CAPABILITY(x) THREAD_ANNOTATION_ATTRIBUTE__ (lock_returned (x))

#define NO_THREAD_SAFETY_ANALYSIS                                              \
  THREAD_ANNOTATION_ATTRIBUTE__ (no_thread_safety_analysis)

typedef struct CAPABILITY ("mutex")
{
  GMutex lock;
} FsmMutex;

void fsm_mutex_init (FsmMutex *mutex);
void fsm_mutex_clear (FsmMutex *mutex);
void fsm_mutex_lock (FsmMutex *mutex) ACQUIRE (mutex) NO_THREAD_SAFETY_ANALYSIS;
void fsm_mutex_unlock (FsmMutex *mutex)
    RELEASE (mutex) NO_THREAD_SAFETY_ANALYSIS;

// Redefine GMutex methods with our own
#ifndef FSM_MUTEX_IMPL

#define GMutex         FsmMutex
#define g_mutex_init   fsm_mutex_init
#define g_mutex_clear  fsm_mutex_init
#define g_mutex_lock   fsm_mutex_lock
#define g_mutex_unlock fsm_mutex_unlock

#endif // FSM_MUTEX_IMPL

#endif // STATIC_THREAD_ANALYSIS

#endif // FSM_MUTEX_H
