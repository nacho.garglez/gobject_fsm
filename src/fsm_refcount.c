/*
 * This is borrowed from GLIB >= 2.58
 * grefcount.c module
 *
 */

#include "fsm_refcount.h"

void
fsm_atomic_ref_count_init (FsmAtomicRefCount *arc)
{
  g_return_if_fail (arc != NULL);
  *arc = 1;
}

void
fsm_atomic_ref_count_inc (FsmAtomicRefCount *arc)
{
  g_return_if_fail (arc != NULL);
  g_return_if_fail (g_atomic_int_get (arc) > 0);
  g_assert (g_atomic_int_get (arc) != G_MAXINT);
  g_atomic_int_inc (arc);
}

gboolean
fsm_atomic_ref_count_dec (FsmAtomicRefCount *arc)
{
  g_return_val_if_fail (arc != NULL, FALSE);
  g_return_val_if_fail (g_atomic_int_get (arc) > 0, FALSE);

  return g_atomic_int_dec_and_test (arc);
}
