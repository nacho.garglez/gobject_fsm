/*
 * GFsm - useful State Machine Library for GLIB
 * Copyright (C) 2020 Nacho Garcia, Pablo Marcos, and others.
 * See AUTHORS file for a complete list.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "fsm_miniobj.h"

void
fsm_miniobj_init (FsmMiniObj *self, GType type)
{
  self->type = type;
  self->free = NULL;
  fsm_atomic_ref_count_init (&self->ref_count);
}

FsmMiniObj *
fsm_miniobj_ref (FsmMiniObj *self)
{
  fsm_atomic_ref_count_inc (&self->ref_count);
  return self;
}

void
fsm_miniobj_unref (FsmMiniObj *self)
{
  if (G_UNLIKELY (fsm_atomic_ref_count_dec (&self->ref_count)))
    {
      if (self->free)
        {
          self->free (self);
        }
      else
        g_free (self);
    }
}
