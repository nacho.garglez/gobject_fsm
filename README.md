
[![License: LGPL v3](https://img.shields.io/badge/License-LGPL%20v3-blue.svg)](https://www.gnu.org/licenses/lgpl-3.0) [![pipeline status](https://gitlab.freedesktop.org/nacho.garglez/gobject_fsm/badges/master/pipeline.svg)](https://gitlab.freedesktop.org/nacho.garglez/gobject_fsm/-/commits/master) [![coverage report](https://gitlab.freedesktop.org/nacho.garglez/gobject_fsm/badges/master/coverage.svg)](https://gitlab.freedesktop.org/nacho.garglez/gobject_fsm/-/commits/master)

# gobject_fsm

A finite state machine implementation with gobject
