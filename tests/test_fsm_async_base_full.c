
#include "../src/fsm_atomics.h"
#include "fsm_async_base.h"
#include <glib.h>
#include <locale.h>

#define TIME_BASE       20000
#define TIME_BASE_HALF  (TIME_BASE / 2)
#define TIME_BASE_QUART (TIME_BASE / 4)

/*------------------------------------------------------
 A Door
------------------------------------------------------*/

enum
{
  DoorStateClose,
  DoorStateOpen,
  _DoorStateLast_
} door_states;

struct _Cuckoo;
typedef struct _Cuckoo Cuckoo;

typedef struct
{
  FsmAsyncBase parent;
  gint angle;
  FSM_ATOMIC gboolean opened;
} Door;

typedef struct
{
  FsmAsyncBaseClass parent;
} DoorClass;

#define DOOR_TYPE (door_get_type ())

static void
door_init (Door *self)
{
  self->opened = FALSE;
  self->angle = 0;
}

static gboolean
door_is_opened (const Door *self)
{
  return self->opened;
}

static void
door_state_close_exec (FsmBase *fsm_base, FsmEventData *event_data)
{
  Door *self = (Door *) fsm_base;
  g_test_message ("Closing door\n");
  while (--self->angle > 0)
    {
      g_usleep (TIME_BASE_QUART);
    }
  self->opened = FALSE;
  g_test_message ("Door closed\n");
}

static void
door_state_open_exec (FsmBase *fsm_base, FsmEventData *event_data)
{
  g_test_message ("Opening door...\n");
  Door *self = (Door *) fsm_base;
  while (++self->angle < 90)
    {
      g_usleep (TIME_BASE_QUART);
    }
  self->opened = TRUE;
  g_test_message ("Door opened\n");
}

static void
door_event_open (Door *self)
{
  BEGIN_EVENT_TRANSITION
  TRANSITION (DoorStateOpen) /* Close  */
  TRANSITION_IGNORED         /* Open */
  SUBMIT_EVENT_TRANSITION_ASYNC (self, _DoorStateLast_, NULL)
}

static void
door_class_init (DoorClass *klass)
{
  BEGIN_STATE_MAP (door)
  STATE (door, close)
  STATE (door, open)

  END_STATE_MAP (door, FSM_BASE_CLASS (klass))
}

static void
door_event_close (Door *self)
{
  BEGIN_EVENT_TRANSITION
  TRANSITION_IGNORED          /* Close  */
  TRANSITION (DoorStateClose) /* Open  */
  SUBMIT_EVENT_TRANSITION_ASYNC (self, _DoorStateLast_, NULL)
}

G_DEFINE_TYPE (Door, door, FSM_ASYNC_BASE_TYPE)

/*------------------------------------------------------
 A Cuckoo
------------------------------------------------------*/
typedef void (*CuckooCompletionCallback) (gconstpointer);

enum
{
  CuckooStateHidden,
  CuckooStateWaitingDoorOpen,
  CuckooStateGoingOutside,
  CuckooStateTweet,
  CuckooStateGoingInside,
  _CuckooStateLast_
} cuckoo_states;

typedef struct _Cuckoo
{
  FsmAsyncBase parent;
  gint counter;
  const Door *door;
  CuckooCompletionCallback callback;
  gconstpointer callback_data;
} Cuckoo;

typedef struct
{
  FsmAsyncBaseClass parent;
} CuckooClass;

#define CUCKOO_TYPE (cuckoo_get_type ())

static void
cuckoo_init (Cuckoo *self)
{
}
static void
cuckoo_register_completion_callback (Cuckoo *self,
                                     CuckooCompletionCallback callback,
                                     gconstpointer callback_data)
{
  self->callback = callback;
  self->callback_data = callback_data;
}

static void
cuckoo_watch_door (Cuckoo *self, const Door *door)
{
  self->door = door;
}

static void
cuckoo_state_hidden_exec (FsmBase *fsm_base, FsmEventData *event_data)
{
  Cuckoo *self = (Cuckoo *) fsm_base;
  g_assert_true (self->callback);
  self->callback (self->callback_data);
}

static void
cuckoo_state_waiting_door_open_exec (FsmBase *fsm_base,
                                     FsmEventData *event_data)
{
  Cuckoo *self = (Cuckoo *) fsm_base;
  while (!door_is_opened (self->door))
    {
      g_usleep (TIME_BASE_QUART);
    }
  INTERNAL_EVENT (self, CuckooStateGoingOutside, NULL)
}

static void
cuckoo_state_going_outside_exec (FsmBase *fsm_base, FsmEventData *event_data)
{
  Cuckoo *self = (Cuckoo *) fsm_base;
  g_test_message ("cuckoo: going outside\n");
  g_usleep (TIME_BASE_HALF);
  INTERNAL_EVENT (self, CuckooStateTweet, NULL)
}

static void
cuckoo_state_tweet_enter (FsmBase *fsm_base, FsmEventData *event_data)
{
  Cuckoo *self = (Cuckoo *) fsm_base;
  self->counter = 0;
}

static void
cuckoo_state_tweet_exec (FsmBase *fsm_base, FsmEventData *event_data)
{
  Cuckoo *self = (Cuckoo *) fsm_base;
  while (self->counter++ < 3)
    {
      g_test_message ("cuckoo: cuckoo-cuckoo!\n");
      g_usleep (TIME_BASE);
    }
  INTERNAL_EVENT (self, CuckooStateGoingInside, NULL)
}

static void
cuckoo_state_going_inside_exec (FsmBase *self, FsmEventData *event_data)
{
  g_test_message ("cuckoo: going inside\n");
  g_usleep (TIME_BASE_HALF);
  INTERNAL_EVENT (self, CuckooStateHidden, NULL)
}

static void
cuckoo_class_init (CuckooClass *klass)
{
  BEGIN_STATE_MAP (cuckoo)
  STATE (cuckoo, hidden)
  STATE (cuckoo, waiting_door_open)
  STATE (cuckoo, going_outside)
  STATE_FULL (cuckoo_state_tweet_exec, cuckoo_state_tweet_enter, NULL, NULL)
  STATE (cuckoo, going_inside)
  END_STATE_MAP (cuckoo, FSM_BASE_CLASS (klass))
}

static void
cuckoo_event_start (Cuckoo *self)
{
  BEGIN_EVENT_TRANSITION
  TRANSITION (CuckooStateWaitingDoorOpen) /* hidden */
  TRANSITION_IGNORED                      /* waiting_door_open */
  TRANSITION_IGNORED                      /* going_outside */
  TRANSITION_IGNORED                      /* tweet */
  TRANSITION_IGNORED                      /* going_inside */
  SUBMIT_EVENT_TRANSITION_ASYNC (self, _CuckooStateLast_, NULL)
}

G_DEFINE_TYPE (Cuckoo, cuckoo, FSM_ASYNC_BASE_TYPE);

/*------------------------------------------------------
 Unit tests
------------------------------------------------------*/

typedef struct
{
  Cuckoo *cuckoo;
  Door *door;
} Clock;

static void
fsm_clock_set_up (Clock *clock, gconstpointer user_data)
{

  clock->cuckoo = g_object_new (CUCKOO_TYPE, NULL);
  clock->door = g_object_new (DOOR_TYPE, NULL);
  cuckoo_watch_door (clock->cuckoo, clock->door);
}

static void
fsm_clock_tear_down (Clock *clock, gconstpointer user_data)
{
  g_clear_object (&clock->cuckoo);
  g_clear_object (&clock->door);
}
static void
test_fsm_test1_cb (gconstpointer user_data)
{
  const Clock *clock = user_data;
  door_event_close (clock->door);
}

static void
test_fsm_test1 (Clock *clock, gconstpointer user_data)
{
  door_event_open (clock->door);
  cuckoo_event_start (clock->cuckoo);
  cuckoo_register_completion_callback (clock->cuckoo, test_fsm_test1_cb, clock);
}

int
main (int argc, char *argv[])
{
  setlocale (LC_ALL, "");
  g_test_init (&argc, &argv, NULL);
  g_test_add ("/FsmAsyncBase/test1", Clock, NULL, fsm_clock_set_up,
              test_fsm_test1, fsm_clock_tear_down);

  return g_test_run ();
}
