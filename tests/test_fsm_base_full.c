#include "fsm_base.h"
#include <glib.h>
#include <locale.h>

/*
 * A test event type that holds a string
 */

typedef struct
{
  FsmEventData base;
  gchar *string;
} StringEventData;

GST_DEFINE_FSM_EVENT_DATA_OBJECT_TYPE (StringEventData, string_event_data);

static void
string_event_data_free (StringEventData *self)
{
  g_free (self->string);
  g_free (self);
}

static StringEventData *
string_event_data_new (const char *string)
{
  StringEventData *new = g_new0 (StringEventData, 1);
  fsm_event_data_init (FSM_EVENT_DATA (new), string_event_data_get_type ());
  new->string = g_strdup (string);
  FSM_EVENT_DATA_SET_FREE_FNC (new, string_event_data_free);
  return new;
}

typedef struct
{
  FsmBase parent;
  gboolean play_guard_pass;
  gboolean play_enter_called;
  gboolean stop_leave_called;
} Player;

typedef struct
{
  FsmBaseClass parent;
} PlayerClass;

#define PLAYER_TYPE (player_get_type ())

static void
player_init (Player *self)
{
  self->play_guard_pass = TRUE;
}

G_DEFINE_TYPE (Player, player, FSM_BASE_TYPE);

/*
  STATES
*/

typedef enum
{
  State_Stop,
  State_Play,
  _State_Last,
} States;

static void
player_state_stop_exec (FsmBase *self, FsmEventData *event_data)
{
  g_test_message ("Stop\n");
}

static void
player_state_stop_enter (FsmBase *self, FsmEventData *event_data)
{
  g_test_message ("Enter Stop\n");
}

static void
player_state_stop_leave (FsmBase *self)
{
  ((Player *) self)->stop_leave_called = TRUE;
  g_test_message ("Leaving Stop\n");
}

static gboolean
player_state_play_guard (FsmBase *self, FsmEventData *event_data)
{
  StringEventData *string_event = (StringEventData *) event_data;
  g_test_message ("Guard playing satus for: %s\n", string_event->string);
  return ((Player *) self)->play_guard_pass;
}

static void
player_state_play_exec (FsmBase *self, FsmEventData *event_data)
{
  StringEventData *string_event = (StringEventData *) event_data;
  g_test_message ("Playing: %s\n", string_event->string);
}

static void
player_state_play_enter (FsmBase *self, FsmEventData *event_data)
{
  StringEventData *string_event = (StringEventData *) event_data;
  g_test_message ("Entering play status for: %s\n", string_event->string);
  ((Player *) self)->play_enter_called = TRUE;
}

static void
player_state_play_leave (FsmBase *self)
{
  g_test_message ("Leaving play status");
}

static gboolean
player_state_stop_guard (FsmBase *self, FsmEventData *event_data)
{
  g_test_message ("Guard stop status");
  return TRUE;
}

/*
  PUBLIC EVENTS
*/

static void
player_event_stop (Player *self)
{
  BEGIN_EVENT_TRANSITION
  TRANSITION_IGNORED      /* Stop */
  TRANSITION (State_Stop) /* Play */
  SUBMIT_EVENT_TRANSITION_SYNC (self, _State_Last, NULL)
}

static void
player_event_play (Player *self, const char *what_song)
{
  StringEventData *string_event = string_event_data_new (what_song);

  BEGIN_EVENT_TRANSITION
  TRANSITION (State_Play) /* Stop */
  TRANSITION_IGNORED      /* Play */
  SUBMIT_EVENT_TRANSITION_SYNC (self, _State_Last, string_event)
}

static void
player_class_init (PlayerClass *klass)
{
  BEGIN_STATE_MAP (player)
  STATE_ALL (player, stop)
  STATE_ALL (player, play)
  END_STATE_MAP (player, FSM_BASE_CLASS (klass))
}

/*------------------------------------------------------
 Unit tests
------------------------------------------------------*/

typedef struct
{
  Player *obj;
} Fixture;

static void
fsm_fixture_set_up (Fixture *fixture, gconstpointer user_data)
{
  g_assert_true (G_TYPE_IS_ABSTRACT (FSM_BASE_TYPE));
  fixture->obj = g_object_new (PLAYER_TYPE, NULL);
}

static void
fsm_fixture_tear_down (Fixture *fixture, gconstpointer user_data)
{
  g_clear_object (&fixture->obj);
}

static void
test_fsm_test1 (Fixture *fixture, gconstpointer user_data)
{
  player_event_play (fixture->obj, "test");
  g_assert_true (fsm_get_current_state (FSM_BASE (fixture->obj)) == State_Play);

  player_event_stop (fixture->obj);
  g_assert_true (fsm_get_current_state (FSM_BASE (fixture->obj)) == State_Stop);
}

static void
test_fsm_test2 (Fixture *fixture, gconstpointer user_data)
{
  player_event_play (fixture->obj, "test");
  g_assert_true (fsm_get_current_state (FSM_BASE (fixture->obj)) == State_Play);

  player_event_play (fixture->obj, "test");
  g_assert_true (fsm_get_current_state (FSM_BASE (fixture->obj)) == State_Play);
}

static void
test_fsm_test3 (Fixture *fixture, gconstpointer user_data)
{
  fixture->obj->play_guard_pass = FALSE;
  player_event_play (fixture->obj, "test");
  g_assert_true (fsm_get_current_state (FSM_BASE (fixture->obj)) == State_Stop);
}

static void
test_fsm_test4 (Fixture *fixture, gconstpointer user_data)
{
  player_event_play (fixture->obj, "test");
  g_assert_true (fixture->obj->play_enter_called);
  g_assert_true (fixture->obj->stop_leave_called);
}

int
main (int argc, char *argv[])
{
  setlocale (LC_ALL, "");
  g_test_init (&argc, &argv, NULL);
  g_test_add ("/FsmBase/test1", Fixture, NULL, fsm_fixture_set_up,
              test_fsm_test1, fsm_fixture_tear_down);
  g_test_add ("/FsmBase/test2", Fixture, NULL, fsm_fixture_set_up,
              test_fsm_test2, fsm_fixture_tear_down);
  g_test_add ("/FsmBase/test3", Fixture, NULL, fsm_fixture_set_up,
              test_fsm_test3, fsm_fixture_tear_down);
  g_test_add ("/FsmBase/test4", Fixture, NULL, fsm_fixture_set_up,
              test_fsm_test4, fsm_fixture_tear_down);
  return g_test_run ();
}
