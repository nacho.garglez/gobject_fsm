#include "fsm_event_data.h"
#include <glib.h>
#include <locale.h>

/*
 * Test event data that holds an integer
 */

typedef struct
{
  FsmEventData parent;
  gint value;
} IntEventData;

GST_DEFINE_FSM_EVENT_DATA_OBJECT_TYPE (IntEventData, int_event);

static IntEventData *
int_event_new (gint value)
{
  IntEventData *new = g_new0 (IntEventData, 1);
  fsm_event_data_init (FSM_EVENT_DATA (new), int_event_get_type ());
  new->value = value;
  return new;
}

/*
 * Test event data that holds a string
 */

static gboolean free_called = FALSE;

typedef struct
{
  FsmEventData parent;
  gchar *string;
} StringEventData;

GST_DEFINE_FSM_EVENT_DATA_OBJECT_TYPE (StringEventData, string_event_data);

static void
string_event_data_free (FsmEventData *event_data)
{
  StringEventData *self = (StringEventData *) event_data;
  g_free (self->string);
  g_free (self);
  free_called = TRUE;
}

static StringEventData *
string_event_data_new (const gchar *string)
{
  StringEventData *new = g_new0 (StringEventData, 1);
  fsm_event_data_init (FSM_EVENT_DATA (new), string_event_data_get_type ());
  new->string = g_strdup (string);
  FSM_EVENT_DATA_SET_FREE_FNC (new, string_event_data_free);
  return new;
}

/*------------------------------------------------------
 Unit tests
------------------------------------------------------*/

static void
test_fsm_event_test1 (void)
{
  IntEventData *test = int_event_new (0xCAFE);
  g_assert_true (test);
  g_assert_true (FSM_MINIOBJ_GET_REFCOUNT (test) == 1);
  fsm_event_data_unref (FSM_EVENT_DATA (test));
}

static void
test_fsm_event_test2 (void)
{
  StringEventData *test = string_event_data_new ("test");
  g_assert_true (test);
  g_assert_true (FSM_MINIOBJ_GET_REFCOUNT (test) == 1);
  fsm_event_data_unref (FSM_EVENT_DATA (test));
}

static void
test_fsm_event_test3 (void)
{
  StringEventData *test = string_event_data_new ("test");
  fsm_event_data_ref (FSM_EVENT_DATA (test));

  g_assert_true (FSM_MINIOBJ_GET_REFCOUNT (test) == 2);
  fsm_event_data_unref (FSM_EVENT_DATA (test));

  g_assert_true (FSM_MINIOBJ_GET_REFCOUNT (test) == 1);
  fsm_event_data_unref (FSM_EVENT_DATA (test));
}

static void
test_fsm_event_test4 (void)
{
  free_called = FALSE;
  StringEventData *test = string_event_data_new ("test");
  g_assert_true (g_strcmp0 (test->string, "test") == 0);
  fsm_event_data_unref (FSM_EVENT_DATA (test));

  g_assert_true (free_called == TRUE);
}

int
main (int argc, char *argv[])
{
  setlocale (LC_ALL, "");
  g_test_init (&argc, &argv, NULL);
  g_test_add_func ("/FsmEventData/test1", test_fsm_event_test1);
  g_test_add_func ("/FsmEventData/test2", test_fsm_event_test2);
  g_test_add_func ("/FsmEventData/test3", test_fsm_event_test3);
  g_test_add_func ("/FsmEventData/test4", test_fsm_event_test4);
  return g_test_run ();
}
