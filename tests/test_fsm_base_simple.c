#include "fsm_base.h"
#include <glib.h>
#include <locale.h>

typedef struct
{
  FsmBase parent;
  gint mt_race_test;
} Player;

typedef struct
{
  FsmBaseClass parent;
} PlayerClass;

#define PLAYER_TYPE (player_get_type ())

static void
player_init (Player *self)
{
  self->mt_race_test = 0;
}

G_DEFINE_TYPE (Player, player, FSM_BASE_TYPE);

/*
  States
*/

typedef enum
{
  State_Stop,
  State_Play,
  State_Pause,
  _State_Last,
} States;

static void
player_state_stop_exec (FsmBase *fsm_base, FsmEventData *event_data)
{
  Player *self = (Player *) fsm_base;
  self->mt_race_test--;
  g_test_message ("Stop\n");
  g_usleep (1000);
}

static void
player_state_play_exec (FsmBase *fsm_base, FsmEventData *event_data)
{
  Player *self = (Player *) fsm_base;
  self->mt_race_test++;
  g_usleep (1000);
  g_test_message ("Play\n");
}

static void
player_state_pause_exec (FsmBase *self, FsmEventData *event_data)
{
  g_usleep (100);
  g_test_message ("Paused\n");
}

/*
  Public events
*/

static void
player_event_stop (Player *self)
{
  BEGIN_EVENT_TRANSITION
  TRANSITION_IGNORED      /* Stop */
  TRANSITION (State_Stop) /* Play */
  TRANSITION (State_Stop) /* Pause */
  SUBMIT_EVENT_TRANSITION_SYNC (self, _State_Last, NULL)
}

static void
player_event_play (Player *self)
{
  BEGIN_EVENT_TRANSITION
  TRANSITION (State_Play) /* Stop */
  TRANSITION_IGNORED      /* Play */
  TRANSITION (State_Play) /* Pause */
  SUBMIT_EVENT_TRANSITION_SYNC (self, _State_Last, NULL)
}

static void
player_event_pause (Player *self)
{
  BEGIN_EVENT_TRANSITION
  TRANSITION_IGNORED       /* Stop */
  TRANSITION (State_Pause) /* Play */
  TRANSITION_IGNORED       /* Pause */
  SUBMIT_EVENT_TRANSITION_SYNC (self, _State_Last, NULL)
}

static void
player_class_init (PlayerClass *klass)
{
  BEGIN_STATE_MAP (player)
  STATE (player, stop)
  STATE (player, play)
  STATE (player, pause)
  END_STATE_MAP (player, FSM_BASE_CLASS (klass))
}

/*------------------------------------------------------
 Unit tests
------------------------------------------------------*/

typedef struct
{
  Player *obj;
} Fixture;

static void
fsm_fixture_set_up (Fixture *fixture, gconstpointer user_data)
{
  g_assert_true (G_TYPE_IS_ABSTRACT (FSM_BASE_TYPE));
  fixture->obj = g_object_new (PLAYER_TYPE, NULL);
}

static void
fsm_fixture_tear_down (Fixture *fixture, gconstpointer user_data)
{
  g_clear_object (&fixture->obj);
}

static void
test_fsm_test1 (Fixture *fixture, gconstpointer user_data)
{
  player_event_play (fixture->obj);
  g_assert_true (fsm_get_current_state (FSM_BASE (fixture->obj)) == State_Play);

  player_event_stop (fixture->obj);
  g_assert_true (fsm_get_current_state (FSM_BASE (fixture->obj)) == State_Stop);

  player_event_pause (fixture->obj);
  g_assert_true (fsm_get_current_state (FSM_BASE (fixture->obj)) == State_Stop);
}

static void
test_fsm_test2 (Fixture *fixture, gconstpointer user_data)
{
  player_event_play (fixture->obj);
  g_assert_true (fsm_get_current_state (FSM_BASE (fixture->obj)) == State_Play);

  player_event_play (fixture->obj);
  g_assert_true (fsm_get_current_state (FSM_BASE (fixture->obj)) == State_Play);

  player_event_pause (fixture->obj);
  g_assert_true (fsm_get_current_state (FSM_BASE (fixture->obj)) ==
                 State_Pause);

  player_event_stop (fixture->obj);
  g_assert_true (fsm_get_current_state (FSM_BASE (fixture->obj)) == State_Stop);
}

static gpointer
tst_client_0 (gpointer data)
{
  int c;
  Player *player = (Player *) data;
  for (c = 0; c < 100; c++)
    {
      g_usleep (900);
      player_event_play (player);
      g_usleep (600);
      player_event_stop (player);
    }
  return NULL;
}
static gpointer
tst_client_1 (gpointer data)
{
  int c;
  Player *player = (Player *) data;
  for (c = 0; c < 100; c++)
    {
      g_usleep (800);
      player_event_play (player);
      g_usleep (200);
      player_event_stop (player);
    }
  return NULL;
}

static void
test_fsm_test3 (Fixture *fixture, gconstpointer user_data)
{
  GThread *client_0, *client_1;
  client_0 = g_thread_new ("client_0", tst_client_0, fixture->obj);
  client_1 = g_thread_new ("client_1", tst_client_1, fixture->obj);
  g_thread_join (client_0);
  g_thread_join (client_1);
}

int
main (int argc, char *argv[])
{
  setlocale (LC_ALL, "");
  g_test_init (&argc, &argv, NULL);
  g_test_add ("/FsmBase/test1", Fixture, NULL, fsm_fixture_set_up,
              test_fsm_test1, fsm_fixture_tear_down);
  g_test_add ("/FsmBase/test2", Fixture, NULL, fsm_fixture_set_up,
              test_fsm_test2, fsm_fixture_tear_down);
  g_test_add ("/FsmBase/test3", Fixture, NULL, fsm_fixture_set_up,
              test_fsm_test3, fsm_fixture_tear_down);
  return g_test_run ();
}
