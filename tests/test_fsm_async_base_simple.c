
#include "fsm_async_base.h"
#include <glib.h>
#include <locale.h>

typedef void (*PlayerCallback) (gpointer);

typedef struct
{
  FsmEventData parent;
  PlayerCallback callback;
  gpointer callback_data;
} StopEventData;

GST_DEFINE_FSM_EVENT_DATA_OBJECT_TYPE (StopEventData, stop_event_data);

static StopEventData *
stop_event_data_new (PlayerCallback callback, gpointer callback_data)
{
  StopEventData *new = g_new0 (StopEventData, 1);
  fsm_event_data_init (FSM_EVENT_DATA (new), stop_event_data_get_type ());
  new->callback = callback;
  new->callback_data = callback_data;
  return new;
}

typedef struct
{
  FsmAsyncBase parent;
} Player;

typedef struct
{
  FsmAsyncBaseClass parent;
} PlayerClass;

#define PLAYER_TYPE (player_get_type ())

static void
player_init (Player *self)
{
}

G_DEFINE_TYPE (Player, player, FSM_ASYNC_BASE_TYPE);

/*
  STATES
*/

typedef enum
{
  State_Stop,
  State_Play,
  State_Pause,
  _State_Last,
} States;

static void
player_state_stop_exec (FsmBase *self, FsmEventData *event_data)
{
  g_test_message ("Now stop\n");
  g_assert_true (event_data);
  StopEventData *stop_event = (StopEventData *) event_data;
  stop_event->callback (stop_event->callback_data);
}

static void
player_state_play_exec (FsmBase *self, FsmEventData *event_data)
{
  g_test_message ("Now playing\n");
}

static void
player_state_pause_exec (FsmBase *self, FsmEventData *event_data)
{
  g_test_message ("Now pause\n");
}

/*
  PUBLIC EVENTS
*/

static void
player_event_stop (Player *self,
                   PlayerCallback callback,
                   gpointer callback_data)
{
  StopEventData *stop_event = stop_event_data_new (callback, callback_data);

  BEGIN_EVENT_TRANSITION
  TRANSITION_IGNORED      /* Stop */
  TRANSITION (State_Stop) /* Play */
  TRANSITION (State_Stop) /* Pause */
  SUBMIT_EVENT_TRANSITION_ASYNC (self, _State_Last, stop_event)
}

static void
player_event_play (Player *self)
{
  BEGIN_EVENT_TRANSITION
  TRANSITION (State_Play) /* Stop */
  TRANSITION_IGNORED      /* Play */
  TRANSITION (State_Play) /* Pause */
  SUBMIT_EVENT_TRANSITION_ASYNC (self, _State_Last, NULL)
}

static void
player_event_pause (Player *self)
{
  BEGIN_EVENT_TRANSITION
  TRANSITION_IGNORED       /* Stop */
  TRANSITION (State_Pause) /* Play */
  TRANSITION_IGNORED       /* Pause */
  SUBMIT_EVENT_TRANSITION_ASYNC (self, _State_Last, NULL)
}

static void
player_class_init (PlayerClass *klass)
{
  BEGIN_STATE_MAP (player)
  STATE (player, stop)
  STATE (player, play)
  STATE (player, pause)
  END_STATE_MAP (player, FSM_BASE_CLASS (klass))
}

/*------------------------------------------------------
 Unit tests
------------------------------------------------------*/

typedef struct
{
  Player *obj;
  gboolean stop_called;
} Fixture;

static void
fsm_fixture_set_up (Fixture *fixture, gconstpointer user_data)
{
  fixture->obj = g_object_new (PLAYER_TYPE, NULL);
  fixture->stop_called = FALSE;
}

static void
fsm_fixture_tear_down (Fixture *fixture, gconstpointer user_data)
{
  g_clear_object (&fixture->obj);
  g_assert_true (fixture->stop_called);
}

static void
test_fsm_test1_callback (gpointer callback_data)
{
  g_assert_true (callback_data);
  Fixture *fixture = (Fixture *) callback_data;
  fixture->stop_called = TRUE;
}

static void
test_fsm_test1 (Fixture *fixture, gconstpointer user_data)
{
  player_event_play (fixture->obj);
  player_event_pause (fixture->obj);
  player_event_stop (fixture->obj, test_fsm_test1_callback, fixture);
}

int
main (int argc, char *argv[])
{
  setlocale (LC_ALL, "");
  g_test_init (&argc, &argv, NULL);
  g_test_add ("/FsmAsyncBase/test1", Fixture, NULL, fsm_fixture_set_up,
              test_fsm_test1, fsm_fixture_tear_down);
  return g_test_run ();
}
